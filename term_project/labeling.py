def mark_blob(data):
    k = 1
    matches = set()
    for i in range(len(data)):
        row = data[i]
        for j in range(len(row)):
            top = 0 if i == 0 else data[i-1][j]
            left = 0 if j == 0 else data[i][j-1]
            if data[i][j] != 0:
                if top == 0 and left == 0:
                    data[i][j] = k
                    k = k + 1
                elif top != 0 or left != 0:
                    if top == 0:
                        data[i][j] = left
                    elif left == 0:
                        data[i][j] = top
                    else:
                        data[i][j] = min(top, left)
                        if top != left:
                            matches.add((data[i][j], max(top, left)))
    return matches

def merge_matches(matches):
    group = []
    matches = [set(i) for i in matches]
    while len(matches) > 0:
        disjoint = True
        pair = matches.pop(0)
        for i, other in enumerate(matches):
            if not pair.isdisjoint(other):
                pair |= matches.pop(i)
                disjoint = False
                break
        if disjoint:
            group.append(pair)
        else:
            matches.append(pair)
    return group


def merge_blob(data, matches):
    matches = merge_matches(matches)
    matches = [list(i) for i in matches]
    N = dict()

    for index, e in enumerate(matches):
        for i in e:
            N[i] = matches[index][0]

    for i, row in enumerate(data):
        for j in range(len(row)):
            if data[i][j] in N:
                data[i][j] = N[data[i][j]]


def blob_labeling(data):
    matches = mark_blob(data)
    merge_blob(data, matches)
