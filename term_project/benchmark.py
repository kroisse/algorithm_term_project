# -*- coding: utf-8 -*-
from glob import glob
import os.path
import time
import csv
from multiprocessing import Pool

from PIL import Image
import numpy

from term_project import segmentation
from term_project import labeling
from term_project import contour
from term_project.utils import downscale, remap


def adaptive_threshold(img):
    pix = numpy.asarray(img.convert('L'))
    start = time.clock()
    result = segmentation.adaptive_threshold(pix, distance=31)
    elapsed = time.clock() - start
    return result, elapsed


def otsu_method(img):
    pix = numpy.asarray(img.convert('L'))
    start = time.clock()
    result = segmentation.otsu_method(pix)
    elapsed = time.clock() - start
    return result, elapsed


def binary_k_means(img):
    pix = numpy.asarray(img.convert('RGB'))
    start = time.clock()
    result = segmentation.k_means(pix, k=2)
    elapsed = time.clock() - start
    result *= 255
    return result, elapsed


def two_pass(pix):
    pix = pix.copy()
    start = time.clock()
    labeling.blob_labeling(pix)
    elapsed = time.clock() - start
    return pix, elapsed


def contour_tracing(pix):
    pix = pix.copy()
    start = time.clock()
    contour.contour_tracing(pix)
    elapsed = time.clock() - start
    return pix, elapsed


segmentations = ['adaptive_threshold', 'otsu_method', 'binary_k_means']
labelings = ['two_pass', 'contour_tracing']


def benchmark_segmentations(filepath, func_name):
    img = Image.open(filepath)
    img = downscale(img, 100000)
    f = globals()[func_name]
    result, elapsed = f(img)
    print('completed: {1}({0!r})'.format(filepath, func_name))
    result_img = Image.fromarray(result, mode='L')
    path = os.path.join('benchmark', func_name)
    os.makedirs(path, mode=0o755, exist_ok=True)
    result_img.save(os.path.join(path, filepath), 'GIF')
    size = result_img.size[0] * result_img.size[1]
    return result, elapsed, size, path, filepath, func_name


def benchmark_labeling(pix, func_name, path, filename):
    pix = pix.astype(numpy.uint16)
    f = globals()[func_name]
    result, elapsed = f(pix)
    print('completed: {1}({0!r})'.format(filename, func_name))
    remapped = remap(result)
    result_img = Image.fromarray(remapped, mode='RGBA')
    path = os.path.join(path, func_name)
    os.makedirs(path, mode=0o755, exist_ok=True)
    result_img.save(os.path.join(path, filename), 'PNG')
    return elapsed


if __name__ == '__main__':
    with Pool(processes=2) as pool:
        files = glob('*.jpg') + glob('*.png')
        result = pool.starmap(benchmark_segmentations,
                              ((p, f) for p in files
                                      for f in segmentations))
        data = [(pix, func_name, path, filename)
                for pix, _, _, path, filename, _ in result
                for func_name in labelings]
        # print(data)
        elapsed = pool.starmap(benchmark_labeling, data)
    data = [i[1:] + (fname,) for i in result for fname in labelings]
    data = [list(i + (e,)) for i, e in zip(data, elapsed)]
    with open('result.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow('elapsed_seg,size,path,filename,segment_algo,labeling_algo,elapsed_label'.split(','))
        writer.writerows(data)
