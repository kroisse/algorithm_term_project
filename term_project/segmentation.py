import numpy

def get_neighbour(pix, y, x, distance):
    d = distance
    return pix[max(0, y - d) : (y + d + 1),
               max(0, x - d) : (x + d + 1)]

def adaptive_threshold_brute(pix, distance=5):
    """주변 픽셀의 평균 밝기를 기준으로 이미지 색상을 이진화한다.
    
    :param pix: 회색조의 원본 이미지
    :type pix: numpy.ndarray
    :param distance: 주변 픽셀의 최대 거리
    :type distance: int
    :return: 이진화된 이미지. 크기는 pix와 같다.
    
    """
    means = numpy.zeros_like(pix)
    for y in range(pix.shape[0]):
        for x in range(pix.shape[1]):
            # pix[y,x] 주변 distance만큼의 배열 영역을 얻고,
            # 그 영역 안의 픽셀 밝기를 평균낸다.
            neighbours = get_neighbour(pix, y, x, distance)
            means[y, x] = neighbours.mean()
    # True == 1, False == 0
    result = ((pix - means) > 0).astype(numpy.uint8)
    return result

def adaptive_threshold(pix, distance=20):
    """주변 픽셀의 평균 밝기를 기준으로 색상을 이진화한다.
    
    :param pix: 회색조의 원본 이미지
    :type pix: numpy.ndarray
    :param distance: 주변 픽셀의 최대 거리
    :type distance: int
    :return: 이진화된 이미지. 크기는 pix와 같다.
    
    """
    result = numpy.zeros_like(pix, dtype=numpy.uint8)
    d = distance
    for y in range(pix.shape[0]):
        # 현재 행의 위아래 distance만큼을 잡고,
        # 세로 방향으로 일제히 평균을 구한다.
        line_avg = numpy.average(pix[max(0, y - d):y + d + 1,:], axis=0)
        # 열이 distance에 비해 충분히 넓으면 최적화된 방법을 사용
        if pix.shape[1] > distance * 3:
            for x in range(distance):
                avg = numpy.average(line_avg[max(0, x - d):x + d + 1])
                result[y, x] = 0 if pix[y, x] < avg else 255
            v = numpy.vstack([line_avg[d+i:len(line_avg)-(d-i)] for i in range(-d, d + 1)])
            result[y, distance:-distance] = numpy.greater(pix[y, distance:-distance], numpy.average(v, axis=0)) * 255
            for x in range(pix.shape[1] - distance, pix.shape[1]):
                avg = numpy.average(line_avg[max(0, x - d):x + d + 1])
                result[y, x] = 0 if pix[y, x] < avg else 255
        else:
            for x in range(pix.shape[1]):
                avg = numpy.average(line_avg[max(0, x - d):x + d + 1])
                result[y, x] = 0 if pix[y, x] < avg else 255
    return result


def otsu_threshold(pix):
    histo = numpy.zeros(256, dtype=float)
    for i in numpy.nditer(pix):
        histo[i] += 1
    histo /= pix.size
    # 범위 합계를 빠르게 구하기 위해 누적분포를 미리 계산한다.
    cweights = histo.cumsum()
    cmeans = (histo * numpy.arange(len(histo))).cumsum()
    vet = []
    for i, j in zip(cweights, cmeans):
        w1, w2 = i, cweights[-1] - i
        m1, m2 = j, cmeans[-1] - j
        diff = w1 * w2 * (m2 - m1) ** 2
        vet.append(diff)
    return max(range(len(vet)), key=lambda x: vet[x])

def otsu_method(pix):
    """픽셀 밝기 히스토그램을 기준으로 이미지 색상을 이진화한다.

    """
    threshold = otsu_threshold(pix)
    it = numpy.nditer([pix, None],
                      op_flags=[['readonly'], ['writeonly', 'allocate']])
    for i, o in it:
        o[...] = 0 if i < threshold else 255
    return it.operands[1]


def euclidian_distance_2(a, b):
    return sum(numpy.abs(a - b) ** 2)

def k_means(pix, k=5):
    """이미지의 색상을 k개로 단순화시킨다.

    RGB 이미지에서 각 픽셀의 색상은 빨강, 초록, 파랑 세 가지 색의 밝기로 결정된다.
    따라서 각 밝기값을 3차원 좌표의 세 축에 대응시키면 R, G, B 세 축을 가지는
    3차원 공간에 픽셀들을 배치할 수 있다. 여기에 K-means 알고리즘을 적용해서 점들을
    k개의 클러스터로 나누면 비슷한 색상끼리 같은 클러스터로 묶이게 된다.

    :param pix:
    :param k:
    :return:

    """
    y, x, depth = pix.shape
    points = pix.reshape((y * x, depth))
    # 각 픽셀별 클러스터 번호를 담을 배열
    z = numpy.arange(len(points))
    z[k:] = k - 1
    step = 1
    while True:
        # 각 클러스터의 중심점을 찾음
        centers = numpy.zeros((k, 3), dtype=int)
        for i in range(k):
            s = z == i
            r = numpy.average(points[s, 0])
            g = numpy.average(points[s, 1])
            b = numpy.average(points[s, 2])
            centers[i] = (r, g, b)
        next_z = numpy.zeros(len(points), dtype=int)
        # 점마다 각 클러스터의 중심점까지의 거리를 구하고,
        # 가장 짧은 거리의 클러스터로 점을 다시 묶는다
        for i, p in enumerate(points):
            d = ((i, euclidian_distance_2(p, j)) for i, j in enumerate(centers))
            m = min(d, key=lambda x: x[1])[0]
            next_z[i] = m
        if numpy.all(z == next_z):
            break
        else:
            z = next_z
            step += 1
    result = numpy.zeros(y * x, dtype=numpy.uint8)
    for i, v in enumerate(z):
        result[i] = v
    return result.reshape((y, x))