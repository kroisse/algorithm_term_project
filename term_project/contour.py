import numpy

# (x+dx, y+dy)
dx = [0, 1, 1, 1, 0, -1, -1, -1]
dy = [-1,-1,0, 1, 1, 1, 0, -1]

D8 = zip(dy, dx)

# 2단계
# 외곽선을 추척하는 함수
# marked는 Labeling이 되었는지 check 하는 배열이고
# marked2는 현재 한 객체 외곽선의 marked 된 번호
def temp_trace(data, i, j, marked, marked2):
    x = 0
    stack = [] # 재귀함수가 stackoverflow가 일어날 가능성 때문에 stack으로 구현
    if 0 < i < len(data) and 0 < j < len(data[i]):
        stack.append((i, j))
        while len(stack) > 0:
            check = 0
            (i, j, ) = stack.pop()
            if 0 < i < len(data) - 2  and 0 < j < len(data[i]) - 2:
                #만약 가장자리 이면
                if data[i][j] != data[i - 1][j] or data[i][j] != data[i + 1][j] or data[i][j] != data[i][j - 1] or \
                                data[i][j] != data[i][j + 1]:
                    marked[i][j] = marked2
                    data[i][j] = marked[i][j]

                    # 다시 외곽선을 추적
                    for x in range(len(dx)):
                        if 0 < i + dy[x] < len(data) and 0 < j + dx[x] < len(data[i]):
                            if data[i + dy[x]][j + dx[x]] != 0 and marked[i + dy[x]][j + dx[x]] == 0:
                                stack.append((i + dy[x], j + dx[x]))

# 1단게 or 4단계
# 처음 번호를 부여받지 않은 객체를 만났을 때
def trace(data, i, j, index, marked):
    x = 0
    if data[i][j] !=0:
        # 만약 외곽선이면
        if data[i][j] != data[i - 1][j] or data[i][j] != data[i + 1][j] or data[i][j] != data[i][j - 1] or \
                                data[i][j] != data[i][j + 1]:
#        if any(data[i][j] != data[i+dy][j+dx] for dy, dx in D4):
            marked[i][j] = index
            data[i][j] = marked[i][j]
            index = index + 1

            for x in range(len(dx)):
                if data[i+dy[x]][j+dx[x]] != 0 and marked[i+dy[x]][j+dx[x]] == 0:
                    temp_trace(data, i+dy[x], j+dx[x], marked, marked[i][j])
                    return index

    return index

def contour_tracing(data):
    marked = numpy.zeros_like(data)
    index = 0
    for i in range(len(data)-1):
        row = data[i]
        for j in range(len(row)-1):
            if data[i][j] != 0 :
                if data[i][j] != marked[i][j] :
                    index = trace(data, i, j, index, marked)

            # 3단계
            # 외곽선 안에 객체를 만나면 그 객체의 외곽선 번호를 붙임
                if marked[i+1][j] != 0 :
                    data[i][j] = marked[i+1][j]
                elif marked[i-1][j] != 0:
                    data[i][j] =  marked[i-1][j]
                elif marked[i][j+1] != 0 :
                    data[i][j] = marked[i][j+1]
                elif marked[i][j-1] != 0:
                    data[i][j] =  marked[i][j-1]