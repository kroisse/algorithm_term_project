from PIL import Image
import numpy
import time
from term_project import segmentation
from term_project.contour import contour_tracing
from term_project.utils import downscale, remap


if __name__ == '__main__':
    orig_img = Image.open('image.jpg')
    img = downscale(orig_img.convert('RGB'), 100000)
    pix = numpy.asarray(img)
    pix = segmentation.k_means(pix, 2)
    # pix = pix.astype(numpy.uint16)

    # blob_labeling(pix)
    # contour_tracing(pix)
    pix = remap(pix)
    img = Image.fromarray(pix, mode='RGBA')
    img = img.resize(orig_img.size, Image.ANTIALIAS)
    # orig_img.paste(img, img)
    img.show()
