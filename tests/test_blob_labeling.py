from term_project.labeling import merge_matches, blob_labeling


def read_data(data):
    a = []
    b = []
    d = data.lstrip().split()
    for i in d:
        for j in i:
            a.append(int(j))
        b.append(a)
        a = []
    return b


def test_read_data():
    data = '''
00000
00110
01111
'''
    result = read_data(data)
    assert result == [[0, 0, 0, 0, 0], [0, 0, 1, 1, 0], [0, 1, 1, 1, 1]]


def test_read_data_2():
    result = read_data('111 000 110')
    assert result == [[1, 1, 1], [0, 0, 0], [1, 1, 0]]


def test_merge_matches():
    assert merge_matches([[1, 2], [1, 3]]) == [{1, 2, 3}]
    assert merge_matches([[1, 2], [3, 5], [4, 7], [7, 8]]) == [{1, 2}, {3, 5}, {4, 7, 8}]


def test_blob_labeling():
    data = '''
111000
101010
000011
001111
'''
    expected = '''
111000
101020
000022
002222
'''
    data = read_data(data)
    expected = read_data(expected)
    blob_labeling(data)
    assert data == expected
