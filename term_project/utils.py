import numpy
from PIL import ImageColor


def remap(arr):
    ids = numpy.unique(arr)
    L = len(ids) - 1
    K = 3
    if len(ids) > K:
        L = L // K + 1
    else:
        K = 1
    d = {}
    for index, i in enumerate(ids[1:]):
        index, r = divmod(index, K)
        hue = index * 360 // L
        lightness = (r + 1) * 25
        color = ImageColor.getrgb("hsl({}, 100%, {}%)".format(hue, lightness))
        d[i] = color
    it = numpy.nditer([arr, None],
                      op_flags=[['readonly'], ['writeonly', 'allocate']],
                      op_dtypes=[arr.dtype, numpy.dtype((numpy.uint8, 4))])
    for x, y in it:
        if x != 0:
            y[...] = d[int(x)] + (255,)
        else:
            y[...] = (0, 0, 0, 255)
    return it.operands[1]


def downscale(img, limit=800000):
    width, height = img.size
    size = width * height
    i = 0
    while size > limit:
        i += 1
        size >>= 2
    return img.resize((width >> i, height >> i))
